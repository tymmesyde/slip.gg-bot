var draws = {
  red: 0,
  black: 0,
  green: 0,
  history: []
};

class Roulette {
  get draws() {
    return draws;
  }

  constructor() {

  }

  getColorSuite() {
    let last = this.draws.history[0];
    let count = 0;
    for(let pocket of this.draws.history) {
      if(pocket == last) {
        count++;
      }else {
        break;
      }
    }
    return {color: last, count: count};
  }

  getColorGap() {
    let gap = Math.abs(this.draws.red-this.draws.black);
    let color = 'red';
    if(this.draws.black < this.draws.red) {
      color = 'black';
    }
    return {color: color, count: gap};
  }

  newDraw(color) {
    this.draws[color]++;
    this.draws.history.unshift(color);
  }

  // nextPocketChance() {
  //   let last = draws.history[0];
  //   let chance = math.fraction('1/2');
  //
  //   for(let pocket of draws.history) {
  //     if(pocket == last) {
  //       chance = math.multiply(chance, math.fraction('18/37'));
  //     }else {
  //       break;
  //     }
  //   }
  //
  //   chance = (chance.n/chance.d) * 100;
  //   return {color: last, chance: chance};
  // }

}

module.exports = Roulette;
