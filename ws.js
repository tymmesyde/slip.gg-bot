const WebSocket = require('ws');
const EventEmitter = require('events').EventEmitter;

var wss;
var URL;

var round;
var user;

var nextRound;
var serverTime;

var eventsList = {};
var eventCounter = 0;
var heartbeat;

class Ws {
  get EVENTS() {
    return this.eventsList;
  }

  set EVENTS(ev) {
    this.eventsList = ev;
  }

  constructor(url, id) {
    this.round = new EventEmitter();
    this.user = new EventEmitter();

    this.nextRound = {
      id: 0,
      time: 0
    };
    this.serverTime = 0;
    this.URL = url;

    // LIST OF EVENTS (MESS)
    this.EVENTS = {
      reply: "phx_reply",
      user: {
        name: "user:"+id,
        bet_outcome: "bet_outcome",
        join: {
          id: 1,
          name: "user:"+id,
          type: "phx_join"
        }
      },
      gameserver: {
        name: "gameserver:roulette",
        round: {
          spun: "spun_round",
          next: "next_round"
        },
        current_rounds: "current_rounds",
        join: {
          id: 2,
          name: "gameserver:roulette",
          type: "phx_join"
        }
      },
      bookkeeper: {
        name: "bookkeeper:roulette",
        join: {
          id: 4,
          name: "bookkeeper:roulette",
          type: "phx_join"
        },
        place: {
          id: 4,
          name: "bookkeeper:roulette",
          type: "place_bet"
        }
      },
      heartbeat: {
        id: 4,
        name: "phoenix",
        type: "heartbeat"
      }
    }
  }

  connect() {
    this.wss = new WebSocket(this.URL);

    this.wss.on('open', () => {
      console.log("Connected to WS.");
      this.sendEvent(this.EVENTS.user.join);
    });

    this.wss.on('message', (msg) => {
      // PARSING EVENT TO EXTRACT DATA
      let ev = this.parseEvent(msg);

      // USER JOIN RESPONSE
      if(ev.name == this.EVENTS.user.name && ev.type == this.EVENTS.reply) {
        console.log("User joined.");
        this._heartBeat();

        // JOIN GAMESERVER/BOOKKEEPER EVENTS
        this.sendEvent(this.EVENTS.gameserver.join);
        this.sendEvent(this.EVENTS.bookkeeper.join);
      }

      // FETCH SERVER TIME
      if(ev.name == this.EVENTS.gameserver.name && ev.type == this.EVENTS.gameserver.current_rounds) {
        let time = ev.payload.server_time;
        let gap = time - new Date();
        this.server = {time: time, gap: gap};
      }

      // SPIN RESULT
      if(ev.name == this.EVENTS.gameserver.name && ev.type == this.EVENTS.gameserver.round.spun) {
        let pocket = ev.payload.roulette_round.pocket;
        this.round.emit('spin', pocket);
      }

      // NEXT ROUND INFO
      if(ev.name == this.EVENTS.gameserver.name && ev.type == this.EVENTS.gameserver.round.next) {
        let id = ev.payload.roulette_round.id;
        let at = Date.parse(ev.payload.roulette_round.spin_at);
        this.nextRound = {id: id, time: at};
      }

      // PLACE BET RESPONSE
      if(ev.name == this.EVENTS.bookkeeper.name && ev.type == this.EVENTS.reply) {
        // console.log(ev.payload.status);
        // console.log(ev.payload.response.errors);
      }

      // BET OUTCOME
      if(ev.name == this.EVENTS.user.name && ev.type == this.EVENTS.user.bet_outcome) {
        let payout = ev.payload.roulette_bet.payout_amount;
        let wager = ev.payload.roulette_bet.wager_amount;
        this.user.emit('update', {payout: payout, wager: wager});
      }
    })

    let that = this;
    this.wss.onclose = function(e) {
    console.log('Socket is closed. Reconnect will be attempted in 1 second.', e.reason);
      setTimeout(function() {
        that.connect();
      }, 1000);
    };

    this.wss.on('error', (err) => {
      console.log('Socket encountered error: ', err.message, 'Closing socket');
    });
  }

  parseEvent(data) {
    let parsed = JSON.parse(data);

    let ev = parsed[2];
    let type = parsed[3];
    let payload = parsed[4];

    return {name: ev, type: type, payload: payload};
  }

  sendEvent(event, payload = {}) {
    let data = [
      event.id[0],
      eventCounter,
      event.name,
      event.type,
      payload
    ]

    this.wss.send(JSON.stringify(data));
    eventCounter++;
  }

  _heartBeat() {
    if(!heartbeat) {
      heartbeat = setInterval(() => {
        // console.log("HEARTBEAT");
        this.sendEvent(this.EVENTS.heartbeat);
      }, 10000)
    }
  }
}

module.exports = Ws;
