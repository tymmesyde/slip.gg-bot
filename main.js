require('dotenv').config()
const chalk = require('chalk');
const math = require('mathjs');

const Util = require('./util');
const Roulette = require('./roulette');
const User = require('./user');
const Ws = require('./ws');

const { TOKEN, ID, CURRENCY_PAIR, MAX_ERROR, TRIGGER_SUITE, WAGER_AMOUNT, DEBUG } = process.env;
const URL = `https://api.slip.gg/socket/websocket?token=${TOKEN}&vsn=2.0.0`;

const roulette = new Roulette();
const wss = new Ws(URL, ID);
wss.connect();

const user = new User();
user.init(TOKEN).then(() => {
  console.log("User initialized.");
  start();
}).catch(err => {
  console.log('ERROR:', err);
});;

var stats = {
  guess: [],
  error: 0,
  valid: 0
}

// 0.005% chance to have 7 times a same color
var date = new Date();
var total = 0;
var gain = 0;
var amount = 0;
var multiplier = 1;
var errors = 0;

function start() {
  total = user.info.btc_balance;

  wss.round.on('spin', (pocket) => {
    console.clear();
    console.log(chalk.cyanBright("   ______   _______    ___  ____ ______"));
    console.log(chalk.cyanBright("  / __/ /  /  _/ _ \\  / _ )/ __ |_  __/"));
    console.log(chalk.cyanBright(" _\\ \\/ /___/ // ___/ / _  / /_/ // /   "));
    console.log(chalk.cyanBright("/___/____/___/_/    /____/\\____//_/    "));
    console.log(chalk.magentaBright("  --- Roulette Bot for Slip.gg ---\n"));
    console.linfo("START TIME", date.toLocaleTimeString());
    console.linfo("ELAPSED TIME", Util.msToTime(new Date() - date));

    amount = (((total * CURRENCY_PAIR) + (gain * CURRENCY_PAIR)) / Math.pow(2, MAX_ERROR)).toFixedDown(2);
    if(WAGER_AMOUNT)
      amount = WAGER_AMOUNT;

    let color = Util.getPocketColor(pocket);
    roulette.newDraw(color);
    let prev = checkPrevGuess(color);
    let guess = guessNextColor(TRIGGER_SUITE);

    showInfo();
    showStats();
    showCurrentRound(color, prev, guess);

    let canStop = false;
    if(guess) {
      placingWager(guess, amount);
    }else {
      if(multiplier == 1) {
        canStop = true;
      }
    }

    if(canStop) {
      console.log(chalk.yellowBright("\n=== YOU CAN NOW STOP ME ==="));
    }else {
      console.log(chalk.redBright("\n=== DO NOT STOP ME NOW ==="));
    }

    console.timer("NEXT ROLL in", 30000);
  })

  wss.user.on('update', (user) => {
    if(DEBUG) {
      console.log(user.payout);
      console.log(user.wager);
      console.log((parseFloat(user.payout) - parseFloat(user.wager)));
    }

    let payout = parseFloat(user.payout);
    let wager = parseFloat(user.wager);
    let income = payout;
    if(payout > 0) {
      income = payout - wager;
    }

    gain += income;
  })
}

function showInfo() {
  console.title("INFO");
  console.linfo("BALANCE", ((total * CURRENCY_PAIR) + (gain * CURRENCY_PAIR)).toFixed(2)+'µ₿');
  console.linfo("GAIN", (gain * CURRENCY_PAIR).toFixed(2)+'µ₿');
  console.linfo("WAGER AMOUNT", amount+'µ₿');
  console.linfo("MAX ERROR", MAX_ERROR);
  console.linfo("TRIGGER SUITE", TRIGGER_SUITE);
}

function showStats() {
  console.title("STATS");

  let gap = roulette.getColorGap();
  let suite = roulette.getColorSuite();
  console.log("GAP:", gap);
  console.log("SUITE:", suite);

  let RED = roulette.draws['red'];
  let BLACK = roulette.draws['black'];
  let GREEN = roulette.draws['green'];

  let total = RED + BLACK + GREEN;
  console.linfo("COUNTS", chalk`{redBright ${RED}} | {gray ${BLACK}} | {greenBright ${GREEN}} | total: ${total}`);

  let tries = stats.error + stats.valid;
  console.linfo("GUESS", `error: ${stats.error} | valid: ${stats.valid} | total: ${tries}`);
}

function showCurrentRound(color, prev, guess) {
  console.title("ROUND RESULT");
  console.linfo("DRAW", Util.getTextColor(color));
  console.linfo("PREV GUESS", prev);
  console.linfo("NEXT GUESS", Util.getTextColor(guess));
  console.linfo("ERRORS", errors+'/'+MAX_ERROR);
  console.linfo("MULTIPLIER", 'x'+multiplier);
}

function placingWager(guess, amount) {
  console.title("PLACING WAGER");
  let current = Date.parse(new Date());
  let server = current + wss.server.gap;
  let next = wss.nextRound.time - server;
  let delay = Math.floor(Math.random()*(20000 - 15000 + 1) + 15000);

  let local_wager_amount = amount * multiplier;
  let wager_amount = (amount * multiplier) / CURRENCY_PAIR;
  console.linfo("OPEN WAGERS", next+'ms');
  console.linfo("FAKE DELAY", delay+'ms');
  console.linfo("WAGER", local_wager_amount+' '+Util.getTextColor(guess)+' (x'+multiplier+')');

  if(DEBUG) {
    console.log('\nDEBUG:');
    console.log(current);
    console.log(server);
    console.log(next);

    console.log(local_wager_amount);
    console.log(wager_amount);
  }

  setTimeout(() => {
    let payload = {
      client_side_id: Util.genId(),
      round_id: wss.nextRound.id,
      status: "placing",
      type: "red_black",
      value: Util.getNumColor(guess),
      local_wager_amount: local_wager_amount,
      wager_amount: wager_amount
    }
    wss.sendEvent(wss.EVENTS.bookkeeper.place, payload);
  }, next + delay);
}

function checkPrevGuess(color) {
  let correct = null;
  let last = stats.guess[0];
  if(last) {
    if(color == last){
      correct = true;
      stats.valid++;
      multiplier = 1;
      errors = 0;
    }else {
      correct = false;
      stats.error++;
      multiplier += multiplier;
      errors++;
    }
  }

  return correct;
}

function guessNextColor(trigger) {
  let gap = roulette.getColorGap();
  let suite = roulette.getColorSuite();

  let guess = null;
  if(suite.count >= trigger) {
    guess = Util.reversePocketColor(suite.color);
  }

  stats.guess.unshift(guess);
  return guess;
}
