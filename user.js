const request = require('request');

var info = {
  id: "",
  public_username: "",
  token: "",
  jwt: "",
  btc_balance: "",
  level: "",
  xp: "",
  next_level_xp: "",
}

class User {
  constructor() {
  }

  init(token) {
    let options = {
      url: 'https://api.slip.gg/api/users/me',
      headers: {
        'authorization': 'Bearer '+token
      }
    };

    return new Promise((resolve, reject) => {
      request(options, (error, response, body) => {
        if(!error) {
          this.info = JSON.parse(body).user;
          resolve();
        }else {
          reject(error);
        }
      });
    })
  }

  getInfo() {
    return this.info;
  }
}

module.exports = User;
