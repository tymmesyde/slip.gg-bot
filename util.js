const chalk = require('chalk');
const uuidv1 = require('uuid/v1');
const { Console } = require('console');

const pockets = {
  red: [1, 3, 5, 7, 9, 12, 14, 16, 18, 19, 21, 23, 25, 27, 30, 32, 34, 36],
  black: [2, 4, 6, 8, 10, 11, 13, 15, 17, 20, 22, 24, 26, 28, 29, 31, 33, 35],
  green: [0]
}

exports.getPocketColor = function(num) {
  if(pockets['red'].indexOf(num) != -1) return 'red';
  if(pockets['black'].indexOf(num) != -1) return 'black';
  return 'green';
}

exports.reversePocketColor = function(color) {
  if(color == 'black') return 'red';
  if(color == 'red') return 'black';
  return '';
}

exports.getTextColor = function(color) {
  if(color == 'red') return chalk.redBright('red');
  if(color == 'black') return chalk.grey('black');
  if(color == 'green') return chalk.greenBright('green');
  return null;
}

// 0 black / 1 red
exports.getNumColor = function(color) {
  if(color == 'red') return 0;
  return 1;
}

exports.genId = function() {
  return uuidv1().replace('-', '');
}

Number.prototype.toFixedDown = function(digits) {
  var re = new RegExp("(\\d+\\.\\d{" + digits + "})(\\d)"),
      m = this.toString().match(re);
  return m ? parseFloat(m[1]) : this.valueOf();
};

exports.msToTime = function(duration) {
  var seconds = parseInt((duration / 1000) % 60),
      minutes = parseInt((duration / (1000 * 60)) % 60),
      hours = parseInt((duration / (1000 * 60 * 60)) % 24);

  hours = (hours < 10) ? "0" + hours : hours;
  minutes = (minutes < 10) ? "0" + minutes : minutes;
  seconds = (seconds < 10) ? "0" + seconds : seconds;

  return hours + ":" + minutes + ":" + seconds;
}

/// CONSOLE
Console.prototype.title = function(title) {
  process.stdout.write(chalk.yellowBright(`\n= ${title} =\n`));
};

Console.prototype.linfo = function(key, text) {
  process.stdout.write(chalk`{white ${key}}: {whiteBright ${text}}\n`);
};

Console.prototype.timer = function(text, duration) {
  if(duration > 0) {
    setTimeout(() => {
      duration = duration - 100;
      process.stdout.clearLine();
      process.stdout.cursorTo(0);
      process.stdout.write(chalk.white(text)+' '+chalk.whiteBright(duration / 1000)+'s');
      Console.prototype.timer(text, duration);
    }, 100);
  }
};
